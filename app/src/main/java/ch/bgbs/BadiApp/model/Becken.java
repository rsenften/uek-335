package ch.bgbs.BadiApp.model;

public class Becken {
    String name;
    double temperatur;

    @Override
    public String toString() {
        return name + ": " + temperatur + " °C";
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemperature(double temperatur) {
        this.temperatur = temperatur;
    }
}
