package ch.bgbs.BadiApp.model;

public class Wochentage {
    String wochentage;

    public Wochentage(String wochentag) {

        this.wochentage = wochentag;
    }

    public void setWochentage(String wochentage) {

        this.wochentage = wochentage;
    }

    @Override
    public String toString() {

        return wochentage;
    }
}
