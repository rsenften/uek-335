package ch.bgbs.BadiApp.model;

import java.util.ArrayList;
import java.util.List;

public class Badi {
    private int id;
    private String name;
    private List<Becken> beckenListe;
    private String kanton;
    private String ort;

    @Override
    public String toString() {
        return name + ' ' + kanton + " (" + ort + ')';
    }

    public Badi(int id, String name, String ort, String kanton) {
        this.id = id;
        this.name = name;
        this.kanton = kanton;
        this.ort = ort;
        this.beckenListe = new ArrayList<Becken>();

    }

    public Badi() {
        this.id = id;
        this.name = name;
        this.kanton = kanton;
        this.ort = ort;
        this.beckenListe = new ArrayList<Becken>();
    }

    public void setBeckenListe(List<Becken> beckenListe) {
        this.beckenListe = beckenListe;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKanton(String kanton) {
        this.kanton = kanton;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public void addBecken(Becken becken) {
        this.beckenListe.add(becken);
    }


    public int getId() {

        return id;
    }

    public String getName() {

        return name;
    }


    public List<Becken> getBecken() {

        return beckenListe;
    }

    public String getOrt() {
        return ort;
    }

}