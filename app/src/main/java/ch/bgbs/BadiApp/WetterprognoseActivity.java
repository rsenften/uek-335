package ch.bgbs.BadiApp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import ch.bgbs.BadiApp.dal.Weekdays;
import ch.bgbs.BadiApp.model.Wetter;
import ch.bgbs.BadiApp.model.Wochentage;

public class WetterprognoseActivity extends AppCompatActivity {
    private static final String WIE_WARM_API_URL = "https://www.wiewarm.ch/api/v1/bad.json/";
    private int badiId;
    private ProgressBar progressBar;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_wetterprognose);
        setTitle("Wetterprognose");
        progressBar = findViewById(R.id.loading_weather_details_progress);
        Intent intent = getIntent();
        badiId = intent.getIntExtra("badiId", 0);
        getWetterTemp(WIE_WARM_API_URL + badiId);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        addDayList();
    }

    private void getWetterTemp(String url) {
        final ArrayAdapter<String> wetterTempAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1);
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Wetter wetter = new Wetter();
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONArray wetterJsonArr = jsonObj.getJSONArray("wetter");
                    for (int i = 0; i < wetterJsonArr.length(); i++ ) {

                        wetter.setTemperatur(wetterJsonArr.getJSONObject(i).getString("wetter_temp"));
                    }
                    wetterTempAdapter.add(wetter.getTemperatur(0));
                    wetterTempAdapter.add(wetter.getTemperatur(1));

                    ListView badiInfoList = findViewById(R.id.wetter_temp);
                    badiInfoList.setAdapter(wetterTempAdapter);
                    progressBar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    generateAlertDialog();
                }

                ListView wetterTempList = findViewById(R.id.wetter_temp);
                wetterTempList.setAdapter(wetterTempAdapter);
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                generateAlertDialog();
            }
        });
        queue.add(stringRequest);
    }

    private void generateAlertDialog() {
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder dialogBuilder;
        dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) { // Closes this activity
                finish();
            }
        });
        dialogBuilder.setMessage("Die Badidetails konnten nicht geladen werden. Versuche es später nochmals.").setTitle("Fehler");
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    private void addDayList() {
        ListView wochentage = findViewById(R.id.wochentage);
        ArrayAdapter<Wochentage> wochentageAdapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1);
        wochentageAdapter.addAll(Weekdays.getAll());
        wochentage.setAdapter(wochentageAdapter);
    }
}
