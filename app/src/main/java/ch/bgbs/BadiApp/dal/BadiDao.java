package ch.bgbs.BadiApp.dal;

import java.util.ArrayList;
import java.util.List;

import ch.bgbs.BadiApp.model.Badi;

public class BadiDao {

    public static List<Badi> getAll() {
        List<Badi> availableBadis = new ArrayList<>();
        availableBadis.add(new Badi(71, "Schwimmbad", "Aarberg", "BE"));
        availableBadis.add(new Badi(27, "Schwimmbad Gruebi", "Adelboden", "BE"));
        availableBadis.add(new Badi(6, "Stadtberner Baeder", "Bern", "BE"));
        availableBadis.add(new Badi(55, "Zürichsee", "", "ZH"));
        availableBadis.add(new Badi(7, "Badebetriebe Thun", "Thun", "BE"));
        availableBadis.add(new Badi(153, "Strandbad Bönigen", "Bönigen", "BE"));
        availableBadis.add(new Badi(219, "Freibad Langnau i.E.", "Langnau i.E.", "BE"));
        availableBadis.add(new Badi(68, "Parkschwimmbad Lyss", "Lyss", "BE"));
        availableBadis.add(new Badi(129, "Parkbad Münsingen", "Münsingen", "BE"));
        availableBadis.add(new Badi(63, "Freibad Ostermundigen", "Ostermundigen", "BE"));
        availableBadis.add(new Badi(145, "Alpamare", "Pfäffikon", "SZ"));

        return availableBadis;
    }
}
