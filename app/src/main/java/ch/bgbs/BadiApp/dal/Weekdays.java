package ch.bgbs.BadiApp.dal;

import java.util.ArrayList;
import java.util.List;

import ch.bgbs.BadiApp.model.Wetter;
import ch.bgbs.BadiApp.model.Wochentage;

public class Weekdays {

    public static List<Wochentage> getAll() {
        List<Wochentage> days = new ArrayList<>();
        days.add(new Wochentage("Heute"));
        days.add(new Wochentage("Morgen"));

        return days;
    }
}
