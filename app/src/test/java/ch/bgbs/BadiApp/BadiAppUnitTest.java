package ch.bgbs.BadiApp;

import org.json.JSONException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import ch.bgbs.BadiApp.dal.BadiDao;
import ch.bgbs.BadiApp.helper.WieWarmJsonParser;
import ch.bgbs.BadiApp.model.Badi;
import ch.bgbs.BadiApp.model.Wetter;

import static junit.framework.TestCase.assertEquals;

public class BadiAppUnitTest {

    private static final String testJSON = "{\n" + " \"badid\": \"9000\",\n" +
            " \"badname\": \"Schwimmbad\",\n" + " \"kanton\": \"BE\",\n" +
            " \"plz\": null,\n" +
            " \"ort\": \"Bümpliz\",\n" + " \"adresse1\": \"Bahnhöheweg 70\",\n" +
            " \"adresse2\": null,\n" + " \"email\": \"info@bbcag.ch\",\n" +
            " \"telefon\": null,\n" +
            " \"www\": \"www.berufsbildungscenter.ch\",\n" +
            " \"long\": null,\n" +
            " \"lat\": null,\n" +
            " \"zeiten\": null,\n" +
            " \"preise\": null,\n" +
            " \"info\": \"Keine speziellen Angaben\",\n" +
            " \"wetterort\": \"Bern\",\n" +
            " \"uv_station_name\": \"Bern\",\n" +
            " \"uv_wert\": 1,\n" +
            " \"uv_date\": \"2017-11-28 00:00:00\",\n" +
            " \"uv_date_pretty\": \"28.11.\",\n" +
            " \"becken\": {\n" + " \"Schwimmbecken\": {\n" +
            " \"beckenid\": 185,\n" +
            " \"beckenname\": \"Schwimmbecken\",\n" +
            " \"temp\": \"22.0\",\n" +
            " \"date\": \"2017-07-04 08:02:00\",\n" +
            " \"typ\": \"Hallenbad\",\n" +
            " \"status\": \"geöffnet\",\n" +
            " \"smskeywords\": \";BEUMPLIZ;\",\n" +
            " \"smsname\": \"Bbc Schwimmbad\",\n" +
            " \"ismain\": \"T \",\n" +
            " \"date_pretty\": \"04.07.\"\n" +
            " }\n" +
            " },\n" +
            " \"bilder\": [],\n" +
            " \"wetter\": [\n" +
            " {\n" +
            " \"wetter_symbol\": 9,\n" +
            " \"wetter_temp\": \"3.0\",\n" +
            " \"wetter_date\": \"2017-11-29 00:00:00\",\n" +
            " \"wetter_date_pretty\": \"29.11.\"\n" + " },\n" +
            " {\n" +
            " \"wetter_symbol\": 15,\n" +
            " \"wetter_temp\": \"4.0\",\n" +
            " \"wetter_date\": \"2017-11-28 00:00:00\",\n" +
            " \"wetter_date_pretty\": \"28.11.\"\n" + " }\n" +
            " ]\n" +
            " }";

private static final String testJSON_noId="{\n"+" ,\n"+
        " \"badname\": \"Schwimmbad\",\n"+
        " \"kanton\": \"BE\",\n"+
        " \"plz\": null,\n"+
        " \"ort\": \"Bümpliz\",\n"+
        " \"adresse1\": \"Bahnhöheweg 70\",\n"+
        " \"adresse2\": null,\n"+" \"email\": \"info@bbcag.ch\",\n"+" \"telefon\": null,\n"+
        " \"www\": \"www.berufsbildungscenter.ch\",\n"+
        " \"long\": null,\n"+
        " \"lat\": null,\n"+
        " \"zeiten\": null,\n"+
        " \"preise\": null,\n"+
        " \"info\": \"Keine speziellen Angaben\",\n"+
        " \"wetterort\": \"Bern\",\n"+
        " \"uv_station_name\": \"Bern\",\n"+" \"uv_wert\": 1,\n"+
        " \"uv_date\": \"2017-11-28 00:00:00\",\n"+
        " \"uv_date_pretty\": \"28.11.\",\n"+
        " \"becken\": {\n"+" \"Schwimmbecken\": {\n"+
        " \"beckenid\": 185,\n"+
        " \"beckenname\": \"Schwimmbecken\",\n"+
        " \"temp\": \"22.0\",\n"+
        " \"date\": \"2017-07-04 08:02:00\",\n"+
        " \"typ\": \"Hallenbad\",\n"+
        " \"status\": \"geöffnet\",\n"+
        " \"smskeywords\": \";BEUMPLIZ;\",\n"+
        " \"smsname\": \"Bbc Schwimmbad\",\n"+
        " \"ismain\": \"T \",\n"+
        " \"date_pretty\": \"04.07.\"\n"+
        " }\n"+
        " },\n"+
        " \"bilder\": [],\n"+
        " \"wetter\": [\n"+
        " {\n"+
        " \"wetter_symbol\": 9,\n"+
        " \"wetter_temp\": \"3.0\",\n"+
        " \"wetter_date\": \"2017-11-29 00:00:00\",\n"+
        " \"wetter_date_pretty\": \"29.11.\"\n"+" },\n"+
        " {\n"+
        " \"wetter_symbol\": 15,\n"+
        " \"wetter_temp\": \"4.0\",\n"+
        " \"wetter_date\": \"2017-11-28 00:00:00\",\n"+
        " \"wetter_date_pretty\": \"28.11.\"\n"+" }\n"+
        " ]\n" + "}";

    @Test
    public void badiDao_BadiGenerationTest() {
        List<Badi> badiList = new ArrayList<Badi>();
        badiList = BadiDao.getAll();
        assertEquals(11, badiList.size());
    }

    @Test
    public void wieWarmJsonParser_createBadiIdTest() {
        Badi testBadi = null;
        try {
            testBadi =
                    WieWarmJsonParser.createBadiFromJsonString(testJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        } // checks if the id from the badi equals the expected 9000


        assertEquals(9000, testBadi.getId());

    }

    @Test
    public void wieWarmJsonParser_createBadiWithoutIdTest() {
        Badi testBadi = null;
        try {
            testBadi = WieWarmJsonParser.createBadiFromJsonString(testJSON_noId);
        } catch (JSONException e) {
            e.printStackTrace();
        } // checks if the id from the badi equals the expected 9000

        assertEquals(null, testBadi);
    }

    @Test
    public void setWetterTemperaturTest() {
        String expected = "test";
        String actual;
        Wetter wetter = new Wetter();
        wetter.setTemperatur("test");
        actual = wetter.getTemperatur(0);
        assertEquals(expected, actual);
    }

    @Test
    public void getWetterTemperaturTest() {
        String expected = "32.2 °C";
        String actual;
        Wetter wetter = new Wetter();
        wetter.setTemperatur("32.2");
        actual = wetter.getTemperatur(0);
        assertEquals(expected, actual);
    }


}